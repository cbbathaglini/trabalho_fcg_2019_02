//     Universidade Federal do Rio Grande do Sul
//             Instituto de Informática
//       Departamento de Informática Aplicada
//
//    INF01047 Fundamentos de Computação Gráfica
//               Prof. Eduardo Gastal
//
//                   LABORATÓRIO 5
//

// Arquivos "headers" padrões de C podem ser incluídos em um
// programa C++, sendo necessário somente adicionar o caractere
// "c" antes de seu nome, e remover o sufixo ".h". Exemplo:
//    #include <stdio.h> // Em C
//  vira
//    #include <cstdio> // Em C++
//
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
// Headers abaixo são específicos de C++
#include <map>
#include <stack>
#include <string>
#include <string.h>
#include <vector>
#include <limits>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <algorithm>

// Headers das bibliotecas OpenGL
#include <glad/glad.h>   // Criação de contexto OpenGL 3.3
#include <GLFW/glfw3.h>  // Criação de janelas do sistema operacional

// Headers da biblioteca GLM: criação de matrizes e vetores.
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/type_ptr.hpp>

// Headers da biblioteca para carregar modelos obj
#include <tiny_obj_loader.h>

#include <stb_image.h>

// Headers locais, definidos na pasta "include/"
#include "utils.h"
#include "matrices.h"

// Estrutura que representa um modelo geométrico carregado a partir de um
// arquivo ".obj". Veja https://en.wikipedia.org/wiki/Wavefront_.obj_file .
struct ObjModel
{
    tinyobj::attrib_t                 attrib;
    std::vector<tinyobj::shape_t>     shapes;
    std::vector<tinyobj::material_t>  materials;

    // Este construtor lê o modelo de um arquivo utilizando a biblioteca tinyobjloader.
    // Veja: https://github.com/syoyo/tinyobjloader
    ObjModel(const char* filename, const char* basepath = NULL, bool triangulate = true)
    {
        printf("Carregando modelo \"%s\"... ", filename);

        std::string err;
        bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filename, basepath, triangulate);

        if (!err.empty())
            fprintf(stderr, "\n%s\n", err.c_str());

        if (!ret)
            throw std::runtime_error("Erro ao carregar modelo.");

        printf("OK.\n");
    }
};


// Declaração de funções utilizadas para pilha de matrizes de modelagem.
void PushMatrix(glm::mat4 M);
void PopMatrix(glm::mat4& M);

// Declaração de várias funções utilizadas em main().  Essas estão definidas
// logo após a definição de main() neste arquivo.
void BuildTrianglesAndAddToVirtualScene(ObjModel*); // Constrói representação de um ObjModel como malha de triângulos para renderização
void ComputeNormals(ObjModel* model); // Computa normais de um ObjModel, caso não existam.
void LoadShadersFromFiles(); // Carrega os shaders de vértice e fragmento, criando um programa de GPU
void LoadTextureImage(const char* filename); // Função que carrega imagens de textura
void DrawVirtualObject(const char* object_name); // Desenha um objeto armazenado em g_VirtualScene
GLuint LoadShader_Vertex(const char* filename);   // Carrega um vertex shader
GLuint LoadShader_Fragment(const char* filename); // Carrega um fragment shader
void LoadShader(const char* filename, GLuint shader_id); // Função utilizada pelas duas acima
GLuint CreateGpuProgram(GLuint vertex_shader_id, GLuint fragment_shader_id); // Cria um programa de GPU
void PrintObjModelInfo(ObjModel*); // Função para debugging

// Declaração de funções auxiliares para renderizar texto dentro da janela
// OpenGL. Estas funções estão definidas no arquivo "textrendering.cpp".
void TextRendering_Init();
float TextRendering_LineHeight(GLFWwindow* window);
float TextRendering_CharWidth(GLFWwindow* window);
void TextRendering_PrintString(GLFWwindow* window, const std::string &str, float x, float y, float scale = 1.0f);
void TextRendering_PrintMatrix(GLFWwindow* window, glm::mat4 M, float x, float y, float scale = 1.0f);
void TextRendering_PrintVector(GLFWwindow* window, glm::vec4 v, float x, float y, float scale = 1.0f);
void TextRendering_PrintMatrixVectorProduct(GLFWwindow* window, glm::mat4 M, glm::vec4 v, float x, float y, float scale = 1.0f);
void TextRendering_PrintMatrixVectorProductMoreDigits(GLFWwindow* window, glm::mat4 M, glm::vec4 v, float x, float y, float scale = 1.0f);
void TextRendering_PrintMatrixVectorProductDivW(GLFWwindow* window, glm::mat4 M, glm::vec4 v, float x, float y, float scale = 1.0f);

// Funções abaixo renderizam como texto na janela OpenGL algumas matrizes e
// outras informações do programa. Definidas após main().
void TextRendering_ShowModelViewProjection(GLFWwindow* window, glm::mat4 projection, glm::mat4 view, glm::mat4 model, glm::vec4 p_model);
void TextRendering_ShowEulerAngles(GLFWwindow* window);
void TextRendering_ShowProjection(GLFWwindow* window);
void TextRendering_ShowFramesPerSecond(GLFWwindow* window);

// Funções callback para comunicação com o sistema operacional e interação do
// usuário. Veja mais comentários nas definições das mesmas, abaixo.
void FramebufferSizeCallback(GLFWwindow* window, int width, int height);
void ErrorCallback(int error, const char* description);
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
void CursorPosCallback(GLFWwindow* window, double xpos, double ypos);
void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);

// Definimos uma estrutura que armazenará dados necessários para renderizar
// cada objeto da cena virtual.
struct SceneObject
{
    std::string  name;        // Nome do objeto
    void*        first_index; // Índice do primeiro vértice dentro do vetor indices[] definido em BuildTrianglesAndAddToVirtualScene()
    int          num_indices; // Número de índices do objeto dentro do vetor indices[] definido em BuildTrianglesAndAddToVirtualScene()
    GLenum       rendering_mode; // Modo de rasterização (GL_TRIANGLES, GL_TRIANGLE_STRIP, etc.)
    GLuint       vertex_array_object_id; // ID do VAO onde estão armazenados os atributos do modelo
    glm::vec3    bbox_min; // Axis-Aligned Bounding Box do objeto
    glm::vec3    bbox_max;
};

// Abaixo definimos variáveis globais utilizadas em várias funções do código.

// A cena virtual é uma lista de objetos nomeados, guardados em um dicionário
// (map).  Veja dentro da função BuildTrianglesAndAddToVirtualScene() como que são incluídos
// objetos dentro da variável g_VirtualScene, e veja na função main() como
// estes são acessados.
std::map<std::string, SceneObject> g_VirtualScene;

// Pilha que guardará as matrizes de modelagem.
std::stack<glm::mat4>  g_MatrixStack;

// Razão de proporção da janela (largura/altura). Veja função FramebufferSizeCallback().
float g_ScreenRatio = 1.0f;

// Ângulos de Euler que controlam a rotação de um dos cubos da cena virtual
float g_AngleX = 0.0f;
float g_AngleY = 0.0f;
float g_AngleZ = 0.0f;

// "g_LeftMouseButtonPressed = true" se o usuário está com o botão esquerdo do mouse
// pressionado no momento atual. Veja função MouseButtonCallback().
bool g_LeftMouseButtonPressed = false;
bool g_RightMouseButtonPressed = false; // Análogo para botão direito do mouse
bool g_MiddleMouseButtonPressed = false; // Análogo para botão do meio do mouse

// Variáveis que definem a câmera em coordenadas esféricas, controladas pelo
// usuário através do mouse (veja função CursorPosCallback()). A posição
// efetiva da câmera é calculada dentro da função main(), dentro do loop de
// renderização.
float g_CameraTheta = 0.0f; // Ângulo no plano ZX em relação ao eixo Z
float g_CameraPhi = 0.0f;   // Ângulo em relação ao eixo Y
float g_CameraDistance = 3.5f; // Distância da câmera para a origem

// Variáveis que controlam rotação do antebraço
float g_ForearmAngleZ = 0.0f;
float g_ForearmAngleX = 0.0f;

// Variáveis que controlam translação do torso
float g_TorsoPositionX = 0.0f;
float g_TorsoPositionY = 0.0f;

// Variável que controla o tipo de projeção utilizada: perspectiva ou ortográfica.
bool g_UsePerspectiveProjection = true;

// Variável que controla se o texto informativo será mostrado na tela.
bool g_ShowInfoText = true;

// Variáveis que definem um programa de GPU (shaders). Veja função LoadShadersFromFiles().
GLuint vertex_shader_id;
GLuint fragment_shader_id;
GLuint program_id = 0;
GLint model_uniform;
GLint view_uniform;
GLint projection_uniform;
GLint object_id_uniform;
GLint bbox_min_uniform;
GLint bbox_max_uniform;

// Número de texturas carregadas pela função LoadTextureImage()
GLuint g_NumLoadedTextures = 0;


//Trabalho 2019/02
//Constantes
bool primeiro_rendering = true;
bool posicionou_jogador = false;
    //teclado
    bool seta_para_cima = false;
    bool seta_para_baixo = false;
    bool seta_para_direita = false;
    bool seta_para_esquerda = false;
    bool tecla_Y = false;
    bool tecla_espaco = false;
    bool tecla_aumentar = false;
    bool tecla_diminuir = false;
    bool tecla_tamanho_padrao = false;
    bool largar_bomba_direita = false;
    bool largar_bomba_esquerda = false;
    bool largar_bomba_frente = false;
    bool largar_bomba_atras = false;
bool colidiu = false;


int contador_andou_vertical = 0; // baixo ou cima
int contador_andou_horizontal =0; // esq ou direita

float angulo_rotacao_y =0.0f;
float escala  = 0.5f;

#define PAREDE 0
#define CAIXA_DESTRUTIVEL 1
#define CAMINHO 2
#define CAIXA_COM_MAIS_BOMBA  3
#define JOGADOR 9
#define VELOCIDADE_MOVIMENTACAO 0.1

#define PARA_BAIXO 0
#define PARA_CIMA 1
#define PARA_DIREITA 2
#define PARA_ESQUERDA 3

#define ESCALA_DOS_OBJETOS 0.5
#define POSICAO_INVALIDA -1

//Estruturas

struct Objetos_cena{
    int tipo_do_objeto;
    float position_max_x;
    float position_min_x;
    float position_max_z;
    float position_min_z;
};

struct Game{
    char name_of_maze[30];
    int **matrix_game;
    int lines;
    int columns;
    int number_of_boxes_with_bombs;
    int number_of_destructible_boxes;
    bool lose_or_win;
};

struct Jogador{
    bool alive;
    int number_of_lifes;
    int number_of_bombs;
    float position_x;
    float position_z;
};



struct Bomb{
    float position_x;
    float position_z;
    int time_to_explosion; // tempo em segundos
    int intensity;
    bool activated;
};

//Funções
Game* gerar_labirinto(Game *game, int linhas, int colunas);
void cria_arquivo(Game *game);
int bounding_box(Jogador *jogador,Game *game,Objetos_cena **objetos_cena,Objetos_cena *obj_jogador,int direcao);
Objetos_cena** gerar_objetos(Objetos_cena **objetos_cena, Game *game);
Jogador *jogador = (Jogador*) malloc(sizeof(Jogador*));
Bomb *bomba = (Bomb*) malloc(sizeof(Bomb*));
void TextRendering_Information_Character(GLFWwindow* window,int numero_bombas_jogador);
void TextRendering_Information_Game(GLFWwindow* window,int numero_caixas_destrutiveis);
bool largar_bomba(Jogador *jogador, Game *game,Objetos_cena **objetos_cena,Objetos_cena *obj_jogador, int direcao);

int main(int argc, char* argv[])
{
    int success = glfwInit();
    if (!success){
        fprintf(stderr, "ERROR: glfwInit() failed.\n");
        std::exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(ErrorCallback);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    #ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow* window;
    window = glfwCreateWindow(800, 600, "INF01047 - TRABALHO FINAL FCG", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        fprintf(stderr, "ERROR: glfwCreateWindow() failed.\n");
        std::exit(EXIT_FAILURE);
    }
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetCursorPosCallback(window, CursorPosCallback);
    glfwSetScrollCallback(window, ScrollCallback);
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);
    FramebufferSizeCallback(window, 800, 600); // Forçamos a chamada do callback acima, para definir g_ScreenRatio.
    const GLubyte *vendor      = glGetString(GL_VENDOR);
    const GLubyte *renderer    = glGetString(GL_RENDERER);
    const GLubyte *glversion   = glGetString(GL_VERSION);
    const GLubyte *glslversion = glGetString(GL_SHADING_LANGUAGE_VERSION);

    printf("GPU: %s, %s, OpenGL %s, GLSL %s\n", vendor, renderer, glversion, glslversion);
    LoadShadersFromFiles();

    // Carregamos duas imagens para serem utilizadas como textura
    //LoadTextureImage("../../data/bomber.png");      // TextureImage0
    LoadTextureImage("../../data/tijolos2.jpg"); // TextureImage1
    LoadTextureImage("../../data/grama.jpg");      // TextureImage2
    LoadTextureImage("../../data/caixa.png"); // TextureImage3

    // Construímos a representação de objetos geométricos através de malhas de triângulos
    //ObjModel jogador_model("../../data/bomberman.obj");
    //ComputeNormals(&jogador_model);
    //BuildTrianglesAndAddToVirtualScene(&jogador_model);

    ObjModel bomba_model("../../data/about_bombs/Bomb.obj");
    ComputeNormals(&bomba_model);
    BuildTrianglesAndAddToVirtualScene(&bomba_model);

    ObjModel spheremodel("../../data/sphere.obj");
    ComputeNormals(&spheremodel);
    BuildTrianglesAndAddToVirtualScene(&spheremodel);

    ObjModel bunnymodel("../../data/bunny.obj");
    ComputeNormals(&bunnymodel);
    BuildTrianglesAndAddToVirtualScene(&bunnymodel);

    ObjModel planemodel("../../data/plane.obj");
    ComputeNormals(&planemodel);
    BuildTrianglesAndAddToVirtualScene(&planemodel);

    ObjModel cubo_obj("../../data/cubo.obj");
    ComputeNormals(&cubo_obj);
    BuildTrianglesAndAddToVirtualScene(&cubo_obj);

    if ( argc > 1 ){
        ObjModel model(argv[1]);
        BuildTrianglesAndAddToVirtualScene(&model);
    }

    // Inicializamos o código para renderização de texto.
    TextRendering_Init();

    // Habilitamos o Z-buffer. Veja slide 108 do documento "Aula_09_Projecoes.pdf".
    glEnable(GL_DEPTH_TEST);

    // Habilitamos o Backface Culling. Veja slides 22-34 do documento "Aula_13_Clipping_and_Culling.pdf".
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);

    // Variáveis auxiliares utilizadas para chamada à função
    // TextRendering_ShowModelViewProjection(), armazenando matrizes 4x4.
    glm::mat4 the_projection;
    glm::mat4 the_model;
    glm::mat4 the_view;

    Game *game;


    jogador->alive = true;
    jogador->number_of_lifes = 5;
    jogador->number_of_bombs = 1;
    jogador->position_x = 0.0f;
    jogador->position_z = 0.0f;
    float position_x=jogador->position_x, position_z = jogador->position_z;
    Objetos_cena **objetos_cena;
    Objetos_cena *obj_jogador = (Objetos_cena*) malloc(sizeof(struct Objetos_cena)) ;
    // Ficamos em loop, renderizando, até que o usuário feche a janela
    while (!glfwWindowShouldClose(window))
    {
        int seconds = (int)glfwGetTime();
        // Aqui executamos as operações de renderização

        // Definimos a cor do "fundo" do framebuffer como branco.  Tal cor é
        // definida como coeficientes RGBA: Red, Green, Blue, Alpha; isto é:
        // Vermelho, Verde, Azul, Alpha (valor de transparência).
        // Conversaremos sobre sistemas de cores nas aulas de Modelos de Iluminação.
        //
        //           R     G     B     A
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(program_id);

        // Computamos a posição da câmera utilizando coordenadas esféricas.  As
        // variáveis g_CameraDistance, g_CameraPhi, e g_CameraTheta são
        // controladas pelo mouse do usuário. Veja as funções CursorPosCallback()
        // e ScrollCallback().
        float r = g_CameraDistance;
        float y = r*sin(g_CameraPhi);
        float z = r*cos(g_CameraPhi)*cos(g_CameraTheta);
        float x = r*cos(g_CameraPhi)*sin(g_CameraTheta);

        // Abaixo definimos as varáveis que efetivamente definem a câmera virtual.
        // Veja slides 172-182 do documento "Aula_08_Sistemas_de_Coordenadas.pdf".
        glm::vec4 camera_position_c  = glm::vec4(x,y,z,1.0f); // Ponto "c", centro da câmera
        glm::vec4 camera_lookat_l    = glm::vec4(0.0f,0.0f,0.0f,1.0f); // Ponto "l", para onde a câmera (look-at) estará sempre olhando
        glm::vec4 camera_view_vector = camera_lookat_l - camera_position_c; // Vetor "view", sentido para onde a câmera está virada
        glm::vec4 camera_up_vector   = glm::vec4(0.0f,1.0f,0.0f,0.0f); // Vetor "up" fixado para apontar para o "céu" (eito Y global)

        // Computamos a matriz "View" utilizando os parâmetros da câmera para
        // definir o sistema de coordenadas da câmera.  Veja slide 186 do documento "Aula_08_Sistemas_de_Coordenadas.pdf".
        glm::mat4 view = Matrix_Camera_View(camera_position_c, camera_view_vector, camera_up_vector);

        // Agora computamos a matriz de Projeção.
        glm::mat4 projection;

        // Note que, no sistema de coordenadas da câmera, os planos near e far
        // estão no sentido negativo! Veja slides 190-193 do documento "Aula_09_Projecoes.pdf".
        float nearplane = -0.1f;  // Posição do "near plane"
        float farplane  = -100.0f; // Posição do "far plane"

        if (g_UsePerspectiveProjection)
        {
            // Projeção Perspectiva.
            // Para definição do field of view (FOV), veja slide 227 do documento "Aula_09_Projecoes.pdf".
            float field_of_view = 3.141592 / 3.0f;
            projection = Matrix_Perspective(field_of_view, g_ScreenRatio, nearplane, farplane);
        }
        else
        {
            // Projeção Ortográfica.
            // Para definição dos valores l, r, b, t ("left", "right", "bottom", "top"),
            // PARA PROJEÇÃO ORTOGRÁFICA veja slide 236 do documento "Aula_09_Projecoes.pdf".
            // Para simular um "zoom" ortográfico, computamos o valor de "t"
            // utilizando a variável g_CameraDistance.
            float t = 1.5f*g_CameraDistance/2.5f;
            float b = -t;
            float r = t*g_ScreenRatio;
            float l = -r;
            projection = Matrix_Orthographic(l, r, b, t, nearplane, farplane);
        }

        glm::mat4 model = Matrix_Identity(); // Transformação identidade de modelagem
        glUniformMatrix4fv(view_uniform       , 1 , GL_FALSE , glm::value_ptr(view));
        glUniformMatrix4fv(projection_uniform , 1 , GL_FALSE , glm::value_ptr(projection));

        #define SPHERE 0
        #define BUNNY  1
        #define PLANE  2
        #define WALL_APPEARENCE  3

        if(primeiro_rendering){
            //cria_arquivo(game);
            game->lines = 10;
            game->columns = 10;
            game->number_of_boxes_with_bombs = 10;
            jogador->number_of_bombs = 1;

            //inicializa o tamanho do labirinto do jogo
            game = gerar_labirinto(game, 10,10);

            objetos_cena = gerar_objetos(objetos_cena,game);
            primeiro_rendering = false;
            posicionou_jogador = false;

        }

        for(int i=0; i<game->lines; i++){
            for(int j=0; j<game->columns; j++){
                if(game->matrix_game[i][j] == PAREDE){
                    model = Matrix_Translate(i,0.0,j)*Matrix_Scale(ESCALA_DOS_OBJETOS,ESCALA_DOS_OBJETOS,ESCALA_DOS_OBJETOS);
                    glUniformMatrix4fv(model_uniform, 1 , GL_FALSE , glm::value_ptr(model));
                    glUniform1i(object_id_uniform, WALL_APPEARENCE);
                    DrawVirtualObject("cubo");
                }
                else if(game->matrix_game[i][j] == CAIXA_DESTRUTIVEL){
                    model = Matrix_Translate(i,0.0,j)*Matrix_Scale(0.4,0.4,0.4);
                    glUniformMatrix4fv(model_uniform, 1 , GL_FALSE , glm::value_ptr(model));
                    glUniform1i(object_id_uniform, SPHERE);
                    DrawVirtualObject("cubo");

                }
                else if(game->matrix_game[i][j] == CAIXA_COM_MAIS_BOMBA){
                    model = Matrix_Translate(i,0.0,j)*Matrix_Scale(0.4,0.4,0.4);
                    glUniformMatrix4fv(model_uniform, 1 , GL_FALSE , glm::value_ptr(model));
                    glUniform1i(object_id_uniform, SPHERE);
                    DrawVirtualObject("sphere");
                }
                else  if(game->matrix_game[i][j] == CAMINHO && posicionou_jogador == false){
                    jogador->position_x= i * ESCALA_DOS_OBJETOS;
                    obj_jogador->position_min_x = jogador->position_x;
                    obj_jogador->position_max_x = i * ESCALA_DOS_OBJETOS + ESCALA_DOS_OBJETOS;
                    obj_jogador->position_max_z = j * ESCALA_DOS_OBJETOS + ESCALA_DOS_OBJETOS;
                    jogador->position_z= j * ESCALA_DOS_OBJETOS;
                    obj_jogador->position_min_z = jogador->position_z;
                    //printf("x: %f  z: %f \n", jogador->position_x, jogador->position_z);

                    position_x = ceil(jogador->position_x);
                    position_z = ceil(jogador->position_z);

                    //printf("x: %f  z: %f \n", position_x, position_z);
                    game->matrix_game[(int)position_x][(int)position_z] = JOGADOR;
                    posicionou_jogador = true;
                }
                model = Matrix_Translate(i,-0.5f,j)*Matrix_Scale(ESCALA_DOS_OBJETOS,ESCALA_DOS_OBJETOS,ESCALA_DOS_OBJETOS);
                glUniformMatrix4fv(model_uniform, 1 , GL_FALSE , glm::value_ptr(model));
                glUniform1i(object_id_uniform, BUNNY);
                DrawVirtualObject("plane");
            }
        }

        if(seta_para_baixo == true) {
            /*for(int i=0; i<game->lines; i++){
                printf(" %d  -> ", i);
                for(int j=0; j<game->columns; j++){
                    printf(" %d ", game->matrix_game[i][j]);
                }
                printf("\n");
            }*/
            angulo_rotacao_y = 90;
            colidiu = bounding_box(jogador,game,objetos_cena,obj_jogador,PARA_BAIXO);
            if(!colidiu){
                position_x += VELOCIDADE_MOVIMENTACAO;
                jogador->position_x += VELOCIDADE_MOVIMENTACAO;
                contador_andou_vertical++;
                int posx = ceil(position_x);
                if(contador_andou_vertical == 6){
                    game->matrix_game[posx-1][(int)position_z] = objetos_cena[posx-1][(int)position_z].tipo_do_objeto;
                    game->matrix_game[posx][(int)position_z] = 9;
                    contador_andou_vertical =0;
                }
                //game->matrix_game[posx][posz] = CAMINHO;
                //game->matrix_game[posx+1][posz] = 9;
            }
            seta_para_baixo = false;
        }
        if(seta_para_cima == true) {
            angulo_rotacao_y = -45;
            colidiu = bounding_box(jogador,game,objetos_cena,obj_jogador,PARA_CIMA);
            if(!colidiu){
                position_x -= VELOCIDADE_MOVIMENTACAO;
                jogador->position_x -= VELOCIDADE_MOVIMENTACAO;
                contador_andou_vertical--;
                int posx = ceil(jogador->position_x);
                if(contador_andou_vertical == -5){
                    game->matrix_game[posx+1][(int)position_z] = objetos_cena[posx+1][(int)position_z].tipo_do_objeto;
                    game->matrix_game[posx][(int)position_z] = 9;
                    contador_andou_vertical = 0;
                }
            }
            seta_para_cima = false;
        }
        if(seta_para_direita == true) {
            angulo_rotacao_y = 180;
            colidiu = bounding_box(jogador,game,objetos_cena,obj_jogador,PARA_DIREITA);
            if(!colidiu){
                position_z -= VELOCIDADE_MOVIMENTACAO;
                jogador->position_z-= VELOCIDADE_MOVIMENTACAO;
                contador_andou_horizontal--;
                int posz = ceil(jogador->position_z);
                if(contador_andou_horizontal == -5){
                    game->matrix_game[(int) position_x][posz+1] = objetos_cena[(int) position_x][posz+1].tipo_do_objeto;
                    game->matrix_game[(int) position_x][posz] = 9;
                    contador_andou_horizontal = 0;
                }
            }
            seta_para_direita = false;
        }
        if(seta_para_esquerda == true) {
            angulo_rotacao_y = 45;
            colidiu = bounding_box(jogador,game,objetos_cena,obj_jogador,PARA_ESQUERDA);
            if(!colidiu){
                position_z += VELOCIDADE_MOVIMENTACAO;
                jogador->position_z+= VELOCIDADE_MOVIMENTACAO;
                contador_andou_horizontal++;
                int posz = ceil(jogador->position_z);
                if(contador_andou_horizontal == 5){
                    game->matrix_game[(int) position_x][posz-1] = objetos_cena[(int) position_x][posz-1].tipo_do_objeto;
                    game->matrix_game[(int) position_x][posz] = 9;
                    contador_andou_horizontal = 0;
                }
            }
            seta_para_esquerda = false;
        }

        if(tecla_espaco == true){
            printf("jog x: %3.2f, z: %3.2f \n", jogador->position_x, jogador->position_z);
            jogador->number_of_bombs--;

            for(int i=0; i<game->number_of_boxes_with_bombs+1; i++){
                printf("bomba %d: \n \t pos_x: %3.2f  pos_z: %3.2f \n", i,bomba[i].position_x, bomba[i].position_z);
                printf("\t intensidade: %d  tempo para explodir: %d \n", bomba[i].intensity, bomba[i].time_to_explosion);
            }

            //largar_a_bomba(jogador, game);

            //garantir que sempre tenha uma bomba
            if(jogador->number_of_bombs == 0){
                jogador->number_of_bombs = 1;
            }

            tecla_espaco = false;
        }

        if(largar_bomba_frente){
            angulo_rotacao_y = 90;
            largar_bomba(jogador, game,objetos_cena, obj_jogador, PARA_BAIXO);
            largar_bomba_frente = false;
        }

        if(largar_bomba_atras){
            angulo_rotacao_y = -45;
            largar_bomba_atras = false;
        }

        if(largar_bomba_direita){
            angulo_rotacao_y = 180;
            largar_bomba_direita = false;
        }

        if(largar_bomba_esquerda){
            angulo_rotacao_y = 45;
            if(largar_bomba(jogador, game,objetos_cena, obj_jogador, PARA_ESQUERDA)){
                printf("largar!");
            }
            largar_bomba_esquerda = false;
                /*int bomba_index =0;
                for(int i=0; i<game->number_of_boxes_with_bombs+1;i++){
                    if(!bomba[i].activated){
                        bomba[i].position_x = position_x;
                        bomba[i].position_z = position_z;
                        bomba[i].intensity =1;
                        bomba[i].time_to_explosion = 5;
                        bomba_index = i;
                        printf("x: %3.2f   z: %3.2f intensity: %d time to explosion: %d ", bomba[i].position_x,
                               bomba[i].position_z,bomba[i].intensity, bomba[i].time_to_explosion);
                    }
                }
                model = Matrix_Translate(bomba[bomba_index].position_x,0.0f,bomba[bomba_index].position_z+1)*
                Matrix_Scale(2,2,2)*
                Matrix_Rotate_Y(0);
                glUniformMatrix4fv(model_uniform, 1 , GL_FALSE , glm::value_ptr(model));
                glUniform1i(object_id_uniform, SPHERE);
                DrawVirtualObject("Bomb");

                if(seconds == bomba[bomba_index].time_to_explosion){
                    if(game->matrix_game[(int)position_x][(int)position_z+1] != PAREDE){
                        game->matrix_game[(int)position_x][(int)position_z+1] = CAMINHO;
                    }

                    if(game->matrix_game[(int)position_x][(int)position_z-1] != PAREDE){
                        game->matrix_game[(int)position_x][(int)position_z-1] = CAMINHO;
                    }
                    if(game->matrix_game[(int)position_x+1][(int)position_z] != PAREDE){
                        game->matrix_game[(int)position_x+1][(int)position_z] = CAMINHO;
                    }
                    if(game->matrix_game[(int)position_x-1][(int)position_z] != PAREDE){
                        game->matrix_game[(int)position_x-1][(int)position_z] = CAMINHO;
                    }


                    bomba[bomba_index].position_x = POSICAO_INVALIDA;
                    bomba[bomba_index].position_z = POSICAO_INVALIDA;
                    bomba[bomba_index].activated = false;
                    bomba[bomba_index].intensity = 0;
                    largar_bomba_esquerda = false;

                }
            }*/


        }

        if(tecla_Y == true){
            angulo_rotacao_y += 1;
            tecla_Y = false;
        }

        if(tecla_aumentar == true){
            escala += 0.1;
            tecla_aumentar = false;
        }

        if(tecla_diminuir == true){
            escala -= 0.1;
            tecla_diminuir = false;
        }

        if(tecla_tamanho_padrao == true){
            escala = 0.5;
            tecla_tamanho_padrao = false;
        }




        model = Matrix_Translate(position_x,0.0f,position_z)*
                Matrix_Scale(escala,escala,escala)*
                Matrix_Rotate_Y(angulo_rotacao_y);
        glUniformMatrix4fv(model_uniform, 1 , GL_FALSE , glm::value_ptr(model));
        glUniform1i(object_id_uniform, BUNNY);
        DrawVirtualObject("bunny");


            //printf("NUMBER: %d\n",game->number_of_boxes_with_bombs);
            TextRendering_Information_Game(window,game->number_of_boxes_with_bombs);
            TextRendering_Information_Character(window,jogador->number_of_bombs);

        //TextRendering_ShowEulerAngles(window);
        //TextRendering_ShowProjection(window);
        //TextRendering_ShowFramesPerSecond(window);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    // Fim do programa
    return 0;
}

bool largar_bomba(Jogador *jogador, Game *game,Objetos_cena **objetos_cena,Objetos_cena *obj_jogador, int direcao){

    int posx = 0, posz =0;

    for(int i=0; i<game->lines; i++){
        for(int j=0; j<game->columns; j++){
            if(game->matrix_game[i][j] == 9){
                posx = i;
                posz = j;
            }
        }
    }
    //printf("FLOATMENTE FALANDO --> pos x: %3.2f   pos z: %3.2f\n", jogador->position_x, jogador->position_z);
    //printf("INTEIRAMENTE FALANDO --> pos x: %d   pos z: %d\n", posx, posz);


        obj_jogador->position_min_z = jogador->position_z;
        obj_jogador->position_max_z = jogador->position_z;
        //printf("FLOATMENTE FALANDO --> min z: %3.2f   max z: %3.2f\n", obj_jogador->position_min_z, obj_jogador->position_max_z);

        //printf("1] %3.2f < ", objetos_cena[posx][posz+1].position_min_z);
        //printf("%3.2f  \n", obj_jogador->position_max_z);

        if(obj_jogador->position_max_z > objetos_cena[posx][posz+1].position_min_z){
            //printf("resultado: %3.2f", obj_jogador->position_max_z- objetos_cena[posx][posz+1].position_min_z);
            if(obj_jogador->position_max_z - objetos_cena[posx][posz+1].position_min_z > 0.1){
                //printf(">>>>>>>aqui_1");
                return true;
            }
        }else{
            //printf("resultado: %3.2f", objetos_cena[posx][posz+1].position_min_z - obj_jogador->position_max_z );
            if(objetos_cena[posx][posz+1].position_min_z - obj_jogador->position_max_z > 0.1){
                //printf(">>>>>>>aqui_2");
                return true;
            }
        }


    //}

    /*if(direcao == PARA_ESQUERDA){

            return false;
        }else if( objetos_cena[posx][posz+1].position_min_z - obj_jogador->position_max_z <= 0){//} -VELOCIDADE_MOVIMENTACAO){
            printf("COLIDIU NA ESQUERDA\n\n");
            return true;
        }
        return false;
    }*/

    /*if(direcao == PARA_BAIXO){
        if(game->matrix_game[(int)jogador->position_x-1][(int)jogador->position_z] == CAMINHO){
            printf("pode largar");
        }
    }*/

    /*if(direcao == PARA_ESQUERDA){
        printf("PARA ESQUERDA");
        posx = (int)jogador->position_x;
        posz = (int)jogador->position_z;
        printf("x: %d  z: %d\n", posx, posz);
        printf("\n%3.2f  --- %3.2f\n",objetos_cena[posx][posz+1].position_min_z,obj_jogador->position_max_z);
        printf("  %d\n", game->matrix_game[posx][posz+1]);
        if(game->matrix_game[posx][posz+1] == CAMINHO){
            printf("pode largar na esquerda");
            jogador->number_of_bombs--;
        }



        /*if(game->matrix_game[posx][posz+1] == CAMINHO){
            printf("---");
            if(objetos_cena[posx][posz+1].position_min_z - obj_jogador->position_max_z > 1){
                printf("pode largar na esquerda");

            }
        }*/
    //}

    if(jogador->number_of_bombs == 0){
        jogador->number_of_bombs = 1;
    }

}

void exibir_matriz(Game *game){
    for(int i=0; i<game->lines; i++){
        printf(" %d  -> ", i);
        for(int j=0; j<game->columns; j++){
            printf(" %d ", game->matrix_game[i][j]);
        }
        printf("\n");
    }

}

int bounding_box(Jogador *jogador,Game *game,Objetos_cena **objetos_cena,Objetos_cena *obj_jogador,int direcao){
    int posx = 0, posz =0;

    for(int i=0; i<game->lines; i++){
        for(int j=0; j<game->columns; j++){
            if(game->matrix_game[i][j] == 9){
                posx = i;
                posz = j;
            }
        }
    }

    obj_jogador->position_max_x = jogador->position_x;
    obj_jogador->position_max_z = jogador->position_z;
    obj_jogador->position_min_x = jogador->position_x - ESCALA_DOS_OBJETOS ;
    obj_jogador->position_min_z = jogador->position_z - ESCALA_DOS_OBJETOS;

    if(direcao == PARA_ESQUERDA){
        /*printf("objetos_cena[posx][posz+1].position_min_z: %3.2f \n", objetos_cena[posx][posz+1].position_min_z);
        printf("game->matrix_game[posx][posz+1]: %d \n" , game->matrix_game[posx][posz+1]);
        printf("  - obj_jogador->position_max_z: %3.2f \n",  obj_jogador->position_max_z);
        printf("RESULT: %3.2f \n\n", objetos_cena[posx][posz+1].position_min_z-obj_jogador->position_max_z );*/
        if(game->matrix_game[posx][posz+1] == CAMINHO){
            return false;
        }else if( objetos_cena[posx][posz+1].position_min_z - obj_jogador->position_max_z <= 0){//} -VELOCIDADE_MOVIMENTACAO){
            printf("COLIDIU NA ESQUERDA\n\n");
            return true;
        }
        return false;
    }

    if(direcao == PARA_DIREITA){
        /*printf("game->matrix_game[posx][posz-1]: %d \n" , game->matrix_game[posx][posz-1]);
        printf("objetos_cena[posx][posz-1].position_min_z: %3.2f \n", objetos_cena[posx][posz-1].position_max_z);
        printf("  - obj_jogador->position_max_z: %3.2f \n",  jogador->position_z);
        printf("RESULT: %3.2f \n\n", jogador->position_z- objetos_cena[posx][posz-1].position_max_z );*/
        if(game->matrix_game[posx][posz-1] == CAMINHO){
            return false;
        }else if(jogador->position_z- objetos_cena[posx][posz-1].position_max_z <= 0){//-VELOCIDADE_MOVIMENTACAO){
            printf("COLIDIU NA DIREITA\n\n");
            return true;
        }
        /*printf("objetos_cena[posx][posz-1].position_min_z: %3.2f - obj_jogador->position_max_z: %3.2f\n",
               objetos_cena[posx][posz-1].position_min_z, obj_jogador->position_max_z);*/
        return false;
    }

    if(direcao == PARA_CIMA){
        /*printf("game->matrix_game[posx-1][posz]: %d \n" , game->matrix_game[posx-1][posz]);
        printf("objetos_cena[posx-1][posz].position_max_x: %3.2f \n", objetos_cena[posx-1][posz].position_max_x);
        printf("  - jogador->position_x: %3.2f \n",  jogador->position_x);
        printf("RESULT: %3.2f \n\n", jogador->position_x - objetos_cena[posx-1][posz].position_max_x);*/
        if(game->matrix_game[posx-1][posz] == CAMINHO){
            return false;
        }else if(jogador->position_x - objetos_cena[posx-1][posz].position_max_x <= 0){//-VELOCIDADE_MOVIMENTACAO){
            printf("COLIDIU EM CIMA\n\n");
            return true;
        }
        return false;
    }

    if(direcao == PARA_BAIXO){

        obj_jogador->position_max_x = jogador->position_x + ESCALA_DOS_OBJETOS;
        obj_jogador->position_max_z = jogador->position_z + ESCALA_DOS_OBJETOS;
        obj_jogador->position_min_x = jogador->position_x ;
        obj_jogador->position_min_z = jogador->position_z ;

        /*printf("obj_jogador->position_max_x: %3.2f \t obj_jogador->position_min_x: %3.2f \n", obj_jogador->position_max_x, obj_jogador->position_min_x);
        printf("game->matrix_game[posx+1][posz]: %d \n" , game->matrix_game[posx+1][posz]);
        printf("objetos_cena[posx+1][posz].position_max_x: %3.2f \n", objetos_cena[posx+1][posz].position_min_x);
        printf("  -  obj_jogador->position_max_x: %3.2f \n",  obj_jogador->position_max_x);
        printf("  -  obj_jogador->position_min_x: %3.2f \n",  obj_jogador->position_min_x);
        printf("  - jogador->position_x: %3.2f \n",  jogador->position_x);
        printf("RESULT: %3.2f \n\n", objetos_cena[posx+1][posz].position_min_x - obj_jogador->position_max_x);*/
        if(game->matrix_game[posx+1][posz] == CAMINHO){
            return false;
        }else if(objetos_cena[posx+1][posz].position_min_x - obj_jogador->position_max_x < 0){
            printf("COLIDIU EMBAIXO\n\n");
            return true;
        }
        return false;
    }
    return false;
}

/*
 * gerar um labirinto com paredes aleatórias, caixas aleatórias, caminhos aleatórios
 *  PAREDE 0
 *  CAIXA_DESTRUTIVEL 1
 *  CAMINHO 2
 *  CAIXA_COM_MAIS_BOMBA  3
 *  JOGADOR  9
 */
Game* gerar_labirinto(Game *game, int linhas, int colunas){
    Game *g = (Game*) malloc(sizeof(struct Game));

    char num_l[3], num_c[3],nome[8];
    sprintf(num_l, "%d", linhas);
    sprintf(num_c, "%d", colunas);

    strcat(nome,num_l);
    strcat(nome,"x");
    strcat(nome,num_c);
    //printf("nome: %s **", nome);
    strcpy(g->name_of_maze,nome);
    //g->name_of_maze = strcpy(g->name_of_maze,nome);
    g->lines  = linhas;
    g->columns = colunas;
    g->lose_or_win = true;
    g->matrix_game = (int**) calloc(linhas,sizeof(int));
    if(g->matrix_game == NULL){
        printf("Error: Insufficient Memory.\n");
        exit (1);
    }

    for ( int i = 0; i < linhas; i++ ) {
      g->matrix_game[i] = (int*) calloc (colunas, sizeof(int));
      if (g->matrix_game[i] == NULL) {
            printf("Error: Insufficient Memory.\n");
            exit (1);
        }
    }

    int i=0, j=0;

    for(i=0; i<linhas; i++){
        for(j=0; j<colunas; j++){
            if(i == 0 || i == linhas-1 || j == 0 || j == colunas-1) g->matrix_game[i][j]  = 0;
            else if(i == 1 || j == colunas-2 || i == linhas -2 || j == 1){
               g->matrix_game[i][j] = 1 + rand() % 2;
            }
            else {
                    g->matrix_game[i][j] = 0 + rand() % 4;
                    if(g->matrix_game[i][j] == CAIXA_COM_MAIS_BOMBA){
                        game->number_of_boxes_with_bombs++;
                        //bomba[]
                        if(game->number_of_boxes_with_bombs == 5){
                           g->matrix_game[i][j] = CAIXA_DESTRUTIVEL;
                           g->number_of_destructible_boxes++;
                        }
                    }
                    if(g->matrix_game[i][j] == CAIXA_DESTRUTIVEL){
                        g->number_of_destructible_boxes++;
                    }
            }


        }
    }

    for(int i =0; i< g->number_of_boxes_with_bombs+1; i++){
        bomba[i].position_x = POSICAO_INVALIDA;
        bomba[i].position_z = POSICAO_INVALIDA;
        bomba[i].activated = false;
        if(i%2 == 0 ){
            bomba[i].intensity = 1;
            bomba[i].time_to_explosion= 3000;//3s
        }
        if(i%2 != 0 ){
            bomba[i].intensity = 2;
            bomba[i].time_to_explosion= 5000; //5s
        }
    }





    // mostrando o labirinto
    for(i=0; i<linhas; i++){
        for(j=0; j<colunas; j++){
            printf(" %4d ",g->matrix_game[i][j]);
        }
        printf("\n");
    }


    return g;

}


/*
 * Inicializa a estrutura dos objetos da cena inserindo o valor máximo do x e do z bem como o valor
 * mínimo dos mesmos para cada um dos objetos do cenário.
 */
Objetos_cena** gerar_objetos(Objetos_cena **objetos_cena, Game *game){

    objetos_cena = (Objetos_cena**) calloc(game->lines,sizeof(Objetos_cena));
            if(objetos_cena == NULL){
                printf("Error: Insufficient Memory.\n");
                exit (1);
            }

            for ( int i = 0; i < game->lines; i++ ) {
              objetos_cena[i] = (Objetos_cena*) calloc (game->columns, sizeof(Objetos_cena));
              if (objetos_cena[i] == NULL) {
                    printf("Error: Insufficient Memory.\n");
                    exit (1);
                }
            }

            for(int i=0; i<game->lines; i++){
                for(int j =0; j<game->columns; j++){
                    if (i ==0){
                        objetos_cena[i][j].position_max_x = ESCALA_DOS_OBJETOS;
                        objetos_cena[i][j].position_min_x = 0.0f;
                        objetos_cena[i][j].tipo_do_objeto = game->matrix_game[i][j];
                    }
                    if(i > 0) {
                        objetos_cena[i][j].position_max_x = objetos_cena[i-1][j].position_max_x + ESCALA_DOS_OBJETOS;
                        objetos_cena[i][j].position_min_x = objetos_cena[i-1][j].position_max_x;
                        objetos_cena[i][j].tipo_do_objeto = game->matrix_game[i][j];
                    }
                    if(j ==0 ){
                        objetos_cena[i][j].position_max_z = ESCALA_DOS_OBJETOS;
                        objetos_cena[i][j].position_min_z = 0.0f;
                        objetos_cena[i][j].tipo_do_objeto = game->matrix_game[i][j];
                    }
                    if(j > 0){
                        objetos_cena[i][j].position_max_z = objetos_cena[i][j-1].position_max_z + ESCALA_DOS_OBJETOS;
                        objetos_cena[i][j].position_min_z = objetos_cena[i][j-1].position_max_z;
                        objetos_cena[i][j].tipo_do_objeto = game->matrix_game[i][j];
                    }
                }
            }

            /*printf("\n");
             for(int i=0; i<game->lines; i++){
                printf("linha %d --> \n", i);
                for(int j =0; j<game->columns; j++){
                    printf("[ TIPO: %d  \t j = %d ",objetos_cena[i][j].tipo_do_objeto,j);
                    printf(" max_x:%3.2f ", objetos_cena[i][j].position_max_x);
                    printf(" max_z:%3.2f ", objetos_cena[i][j].position_max_z);
                    printf(" min_x:%3.2f ", objetos_cena[i][j].position_min_x);
                    printf(" min_z:%3.2f ]\n", objetos_cena[i][j].position_min_z);
                }
                printf("\n\n");
             }*/

    return objetos_cena;
}

void cria_arquivo(Game *game){
     FILE *pont_arq; // cria variável ponteiro para o arquivo
      char palavra[20], caminho[20];

    strcpy(palavra,game->name_of_maze);
    strcat(palavra,".txt");
    printf("palavara: %s **",palavra);
    strcpy(caminho, "../../data/");
    strcat(caminho,palavra);

      //abrindo o arquivo com tipo de abertura w
      pont_arq = fopen(caminho, "w");
      //printf("caminho: %s **", caminho);

      //testando se o arquivo foi realmente criado
      if(pont_arq == NULL){
        printf("Erro na abertura do arquivo!");
        return exit(EXIT_FAILURE);
      }

      //usando fprintf para armazenar a string no arquivo
      fprintf(pont_arq, "%d", game->lines);
      fprintf(pont_arq, "%s", "x");
      fprintf(pont_arq, "%d", game->columns);
      fprintf(pont_arq, "%s", "\n");
      for(int i=0; i<game->lines; i++){
        for(int j=0; j<game->columns; j++){
            fprintf(pont_arq, "%d", game->matrix_game[i][j]);
        }
        fprintf(pont_arq, "%s", "\n");
    }


      //usando fclose para fechar o arquivo
      fclose(pont_arq);
}

void TextRendering_Information_Character(GLFWwindow* window,int numero_bombas_jogador){
    if ( !g_ShowInfoText )
        return;

    float pad = TextRendering_LineHeight(window);

    char buffer[80];
    float lineheight = TextRendering_LineHeight(window);
    float charwidth = TextRendering_CharWidth(window);



    char numchars = snprintf(buffer, 80, "Number of bombs: %d \n", numero_bombas_jogador);
    TextRendering_PrintString(window, buffer, 1.0f-(numchars + 1)*charwidth, 1.00f-lineheight, 1.0f);

}

void TextRendering_Information_Game(GLFWwindow* window,int numero_caixas_destrutiveis){
    if ( !g_ShowInfoText )
        return;

    float pad = TextRendering_LineHeight(window);

    char buffer[80];
    float lineheight = TextRendering_LineHeight(window);
    float charwidth = TextRendering_CharWidth(window);
    char numchars = snprintf(buffer, 80, "Number of destructible boxes: %d \n", numero_caixas_destrutiveis);
    TextRendering_PrintString(window, buffer, 0.5f-(numchars + 1)*charwidth, 1.0f-lineheight, 1.0f);

}

// Função que carrega uma imagem para ser utilizada como textura
void LoadTextureImage(const char* filename)
{
    printf("Carregando imagem \"%s\"... ", filename);

    // Primeiro fazemos a leitura da imagem do disco
    stbi_set_flip_vertically_on_load(true);
    int width;
    int height;
    int channels;
    unsigned char *data = stbi_load(filename, &width, &height, &channels, 3);

    if ( data == NULL )
    {
        fprintf(stderr, "ERROR: Cannot open image file \"%s\".\n", filename);
        std::exit(EXIT_FAILURE);
    }

    printf("OK (%dx%d).\n", width, height);

    // Agora criamos objetos na GPU com OpenGL para armazenar a textura
    GLuint texture_id;
    GLuint sampler_id;
    glGenTextures(1, &texture_id);
    glGenSamplers(1, &sampler_id);

    // Veja slide 100 do documento "Aula_20_e_21_Mapeamento_de_Texturas.pdf"
    glSamplerParameteri(sampler_id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(sampler_id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Parâmetros de amostragem da textura. Falaremos sobre eles em uma próxima aula.
    glSamplerParameteri(sampler_id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(sampler_id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Agora enviamos a imagem lida do disco para a GPU
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

    GLuint textureunit = g_NumLoadedTextures;
    glActiveTexture(GL_TEXTURE0 + textureunit);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindSampler(textureunit, sampler_id);

    stbi_image_free(data);

    g_NumLoadedTextures += 1;
}

// Função que desenha um objeto armazenado em g_VirtualScene. Veja definição
// dos objetos na função BuildTrianglesAndAddToVirtualScene().
void DrawVirtualObject(const char* object_name)
{
    // "Ligamos" o VAO. Informamos que queremos utilizar os atributos de
    // vértices apontados pelo VAO criado pela função BuildTrianglesAndAddToVirtualScene(). Veja
    // comentários detalhados dentro da definição de BuildTrianglesAndAddToVirtualScene().
    glBindVertexArray(g_VirtualScene[object_name].vertex_array_object_id);

    // Setamos as variáveis "bbox_min" e "bbox_max" do fragment shader
    // com os parâmetros da axis-aligned bounding box (AABB) do modelo.
    glm::vec3 bbox_min = g_VirtualScene[object_name].bbox_min;
    glm::vec3 bbox_max = g_VirtualScene[object_name].bbox_max;
    glUniform4f(bbox_min_uniform, bbox_min.x, bbox_min.y, bbox_min.z, 1.0f);
    glUniform4f(bbox_max_uniform, bbox_max.x, bbox_max.y, bbox_max.z, 1.0f);

    // Pedimos para a GPU rasterizar os vértices dos eixos XYZ
    // apontados pelo VAO como linhas. Veja a definição de
    // g_VirtualScene[""] dentro da função BuildTrianglesAndAddToVirtualScene(), e veja
    // a documentação da função glDrawElements() em
    // http://docs.gl/gl3/glDrawElements.
    glDrawElements(
        g_VirtualScene[object_name].rendering_mode,
        g_VirtualScene[object_name].num_indices,
        GL_UNSIGNED_INT,
        (void*)g_VirtualScene[object_name].first_index
    );

    // "Desligamos" o VAO, evitando assim que operações posteriores venham a
    // alterar o mesmo. Isso evita bugs.
    glBindVertexArray(0);
}

// Função que carrega os shaders de vértices e de fragmentos que serão
// utilizados para renderização. Veja slides 217-219 do documento "Aula_03_Rendering_Pipeline_Grafico.pdf".
//
void LoadShadersFromFiles()
{
    // Note que o caminho para os arquivos "shader_vertex.glsl" e
    // "shader_fragment.glsl" estão fixados, sendo que assumimos a existência
    // da seguinte estrutura no sistema de arquivos:
    //
    //    + FCG_Lab_01/
    //    |
    //    +--+ bin/
    //    |  |
    //    |  +--+ Release/  (ou Debug/ ou Linux/)
    //    |     |
    //    |     o-- main.exe
    //    |
    //    +--+ src/
    //       |
    //       o-- shader_vertex.glsl
    //       |
    //       o-- shader_fragment.glsl
    //
    vertex_shader_id = LoadShader_Vertex("../../src/shader_vertex.glsl");
    fragment_shader_id = LoadShader_Fragment("../../src/shader_fragment.glsl");

    // Deletamos o programa de GPU anterior, caso ele exista.
    if ( program_id != 0 )
        glDeleteProgram(program_id);

    // Criamos um programa de GPU utilizando os shaders carregados acima.
    program_id = CreateGpuProgram(vertex_shader_id, fragment_shader_id);

    // Buscamos o endereço das variáveis definidas dentro do Vertex Shader.
    // Utilizaremos estas variáveis para enviar dados para a placa de vídeo
    // (GPU)! Veja arquivo "shader_vertex.glsl" e "shader_fragment.glsl".
    model_uniform           = glGetUniformLocation(program_id, "model"); // Variável da matriz "model"
    view_uniform            = glGetUniformLocation(program_id, "view"); // Variável da matriz "view" em shader_vertex.glsl
    projection_uniform      = glGetUniformLocation(program_id, "projection"); // Variável da matriz "projection" em shader_vertex.glsl
    object_id_uniform       = glGetUniformLocation(program_id, "object_id"); // Variável "object_id" em shader_fragment.glsl
    bbox_min_uniform        = glGetUniformLocation(program_id, "bbox_min");
    bbox_max_uniform        = glGetUniformLocation(program_id, "bbox_max");

    // Variáveis em "shader_fragment.glsl" para acesso das imagens de textura
    glUseProgram(program_id);
    glUniform1i(glGetUniformLocation(program_id, "TextureImage0"), 0);
    glUniform1i(glGetUniformLocation(program_id, "TextureImage1"), 1);
    glUniform1i(glGetUniformLocation(program_id, "TextureImage2"), 2);
    glUseProgram(0);
}

// Função que pega a matriz M e guarda a mesma no topo da pilha
void PushMatrix(glm::mat4 M)
{
    g_MatrixStack.push(M);
}

// Função que remove a matriz atualmente no topo da pilha e armazena a mesma na variável M
void PopMatrix(glm::mat4& M)
{
    if ( g_MatrixStack.empty() )
    {
        M = Matrix_Identity();
    }
    else
    {
        M = g_MatrixStack.top();
        g_MatrixStack.pop();
    }
}

// Função que computa as normais de um ObjModel, caso elas não tenham sido
// especificadas dentro do arquivo ".obj"
void ComputeNormals(ObjModel* model)
{
    if ( !model->attrib.normals.empty() )
        return;

    // Primeiro computamos as normais para todos os TRIÂNGULOS.
    // Segundo, computamos as normais dos VÉRTICES através do método proposto
    // por Gouraud, onde a normal de cada vértice vai ser a média das normais de
    // todas as faces que compartilham este vértice.

    size_t num_vertices = model->attrib.vertices.size() / 3;

    std::vector<int> num_triangles_per_vertex(num_vertices, 0);
    std::vector<glm::vec4> vertex_normals(num_vertices, glm::vec4(0.0f,0.0f,0.0f,0.0f));

    for (size_t shape = 0; shape < model->shapes.size(); ++shape)
    {
        size_t num_triangles = model->shapes[shape].mesh.num_face_vertices.size();

        for (size_t triangle = 0; triangle < num_triangles; ++triangle)
        {
            assert(model->shapes[shape].mesh.num_face_vertices[triangle] == 3);

            glm::vec4  vertices[3];
            for (size_t vertex = 0; vertex < 3; ++vertex)
            {
                tinyobj::index_t idx = model->shapes[shape].mesh.indices[3*triangle + vertex];
                const float vx = model->attrib.vertices[3*idx.vertex_index + 0];
                const float vy = model->attrib.vertices[3*idx.vertex_index + 1];
                const float vz = model->attrib.vertices[3*idx.vertex_index + 2];
                vertices[vertex] = glm::vec4(vx,vy,vz,1.0);
            }

            const glm::vec4  a = vertices[0];
            const glm::vec4  b = vertices[1];
            const glm::vec4  c = vertices[2];

            const glm::vec4  n = crossproduct(b-a,c-a);

            for (size_t vertex = 0; vertex < 3; ++vertex)
            {
                tinyobj::index_t idx = model->shapes[shape].mesh.indices[3*triangle + vertex];
                num_triangles_per_vertex[idx.vertex_index] += 1;
                vertex_normals[idx.vertex_index] += n;
                model->shapes[shape].mesh.indices[3*triangle + vertex].normal_index = idx.vertex_index;
            }
        }
    }

    model->attrib.normals.resize( 3*num_vertices );

    for (size_t i = 0; i < vertex_normals.size(); ++i)
    {
        glm::vec4 n = vertex_normals[i] / (float)num_triangles_per_vertex[i];
        n /= norm(n);
        model->attrib.normals[3*i + 0] = n.x;
        model->attrib.normals[3*i + 1] = n.y;
        model->attrib.normals[3*i + 2] = n.z;
    }
}

// Constrói triângulos para futura renderização a partir de um ObjModel.
void BuildTrianglesAndAddToVirtualScene(ObjModel* model)
{
    GLuint vertex_array_object_id;
    glGenVertexArrays(1, &vertex_array_object_id);
    glBindVertexArray(vertex_array_object_id);

    std::vector<GLuint> indices;
    std::vector<float>  model_coefficients;
    std::vector<float>  normal_coefficients;
    std::vector<float>  texture_coefficients;

    for (size_t shape = 0; shape < model->shapes.size(); ++shape)
    {
        size_t first_index = indices.size();
        size_t num_triangles = model->shapes[shape].mesh.num_face_vertices.size();

        const float minval = std::numeric_limits<float>::min();
        const float maxval = std::numeric_limits<float>::max();

        glm::vec3 bbox_min = glm::vec3(maxval,maxval,maxval);
        glm::vec3 bbox_max = glm::vec3(minval,minval,minval);

        for (size_t triangle = 0; triangle < num_triangles; ++triangle)
        {
            assert(model->shapes[shape].mesh.num_face_vertices[triangle] == 3);

            for (size_t vertex = 0; vertex < 3; ++vertex)
            {
                tinyobj::index_t idx = model->shapes[shape].mesh.indices[3*triangle + vertex];

                indices.push_back(first_index + 3*triangle + vertex);

                const float vx = model->attrib.vertices[3*idx.vertex_index + 0];
                const float vy = model->attrib.vertices[3*idx.vertex_index + 1];
                const float vz = model->attrib.vertices[3*idx.vertex_index + 2];
                //printf("tri %d vert %d = (%.2f, %.2f, %.2f)\n", (int)triangle, (int)vertex, vx, vy, vz);
                model_coefficients.push_back( vx ); // X
                model_coefficients.push_back( vy ); // Y
                model_coefficients.push_back( vz ); // Z
                model_coefficients.push_back( 1.0f ); // W

                bbox_min.x = std::min(bbox_min.x, vx);
                bbox_min.y = std::min(bbox_min.y, vy);
                bbox_min.z = std::min(bbox_min.z, vz);
                bbox_max.x = std::max(bbox_max.x, vx);
                bbox_max.y = std::max(bbox_max.y, vy);
                bbox_max.z = std::max(bbox_max.z, vz);

                // Inspecionando o código da tinyobjloader, o aluno Bernardo
                // Sulzbach (2017/1) apontou que a maneira correta de testar se
                // existem normais e coordenadas de textura no ObjModel é
                // comparando se o índice retornado é -1. Fazemos isso abaixo.

                if ( idx.normal_index != -1 )
                {
                    const float nx = model->attrib.normals[3*idx.normal_index + 0];
                    const float ny = model->attrib.normals[3*idx.normal_index + 1];
                    const float nz = model->attrib.normals[3*idx.normal_index + 2];
                    normal_coefficients.push_back( nx ); // X
                    normal_coefficients.push_back( ny ); // Y
                    normal_coefficients.push_back( nz ); // Z
                    normal_coefficients.push_back( 0.0f ); // W
                }

                if ( idx.texcoord_index != -1 )
                {
                    const float u = model->attrib.texcoords[2*idx.texcoord_index + 0];
                    const float v = model->attrib.texcoords[2*idx.texcoord_index + 1];
                    texture_coefficients.push_back( u );
                    texture_coefficients.push_back( v );
                }
            }
        }

        size_t last_index = indices.size() - 1;

        SceneObject theobject;
        theobject.name           = model->shapes[shape].name;
        theobject.first_index    = (void*)first_index; // Primeiro índice
        theobject.num_indices    = last_index - first_index + 1; // Número de indices
        theobject.rendering_mode = GL_TRIANGLES;       // Índices correspondem ao tipo de rasterização GL_TRIANGLES.
        theobject.vertex_array_object_id = vertex_array_object_id;

        theobject.bbox_min = bbox_min;
        theobject.bbox_max = bbox_max;

        g_VirtualScene[model->shapes[shape].name] = theobject;
    }

    GLuint VBO_model_coefficients_id;
    glGenBuffers(1, &VBO_model_coefficients_id);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_model_coefficients_id);
    glBufferData(GL_ARRAY_BUFFER, model_coefficients.size() * sizeof(float), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, model_coefficients.size() * sizeof(float), model_coefficients.data());
    GLuint location = 0; // "(location = 0)" em "shader_vertex.glsl"
    GLint  number_of_dimensions = 4; // vec4 em "shader_vertex.glsl"
    glVertexAttribPointer(location, number_of_dimensions, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(location);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    if ( !normal_coefficients.empty() )
    {
        GLuint VBO_normal_coefficients_id;
        glGenBuffers(1, &VBO_normal_coefficients_id);
        glBindBuffer(GL_ARRAY_BUFFER, VBO_normal_coefficients_id);
        glBufferData(GL_ARRAY_BUFFER, normal_coefficients.size() * sizeof(float), NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, normal_coefficients.size() * sizeof(float), normal_coefficients.data());
        location = 1; // "(location = 1)" em "shader_vertex.glsl"
        number_of_dimensions = 4; // vec4 em "shader_vertex.glsl"
        glVertexAttribPointer(location, number_of_dimensions, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(location);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    if ( !texture_coefficients.empty() )
    {
        GLuint VBO_texture_coefficients_id;
        glGenBuffers(1, &VBO_texture_coefficients_id);
        glBindBuffer(GL_ARRAY_BUFFER, VBO_texture_coefficients_id);
        glBufferData(GL_ARRAY_BUFFER, texture_coefficients.size() * sizeof(float), NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, texture_coefficients.size() * sizeof(float), texture_coefficients.data());
        location = 2; // "(location = 1)" em "shader_vertex.glsl"
        number_of_dimensions = 2; // vec2 em "shader_vertex.glsl"
        glVertexAttribPointer(location, number_of_dimensions, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(location);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    GLuint indices_id;
    glGenBuffers(1, &indices_id);

    // "Ligamos" o buffer. Note que o tipo agora é GL_ELEMENT_ARRAY_BUFFER.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indices.size() * sizeof(GLuint), indices.data());
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // XXX Errado!
    //

    // "Desligamos" o VAO, evitando assim que operações posteriores venham a
    // alterar o mesmo. Isso evita bugs.
    glBindVertexArray(0);
}

// Carrega um Vertex Shader de um arquivo GLSL. Veja definição de LoadShader() abaixo.
GLuint LoadShader_Vertex(const char* filename)
{
    // Criamos um identificador (ID) para este shader, informando que o mesmo
    // será aplicado nos vértices.
    GLuint vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);

    // Carregamos e compilamos o shader
    LoadShader(filename, vertex_shader_id);

    // Retorna o ID gerado acima
    return vertex_shader_id;
}

// Carrega um Fragment Shader de um arquivo GLSL . Veja definição de LoadShader() abaixo.
GLuint LoadShader_Fragment(const char* filename)
{
    // Criamos um identificador (ID) para este shader, informando que o mesmo
    // será aplicado nos fragmentos.
    GLuint fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);

    // Carregamos e compilamos o shader
    LoadShader(filename, fragment_shader_id);

    // Retorna o ID gerado acima
    return fragment_shader_id;
}

// Função auxilar, utilizada pelas duas funções acima. Carrega código de GPU de
// um arquivo GLSL e faz sua compilação.
void LoadShader(const char* filename, GLuint shader_id)
{
    // Lemos o arquivo de texto indicado pela variável "filename"
    // e colocamos seu conteúdo em memória, apontado pela variável
    // "shader_string".
    std::ifstream file;
    try {
        file.exceptions(std::ifstream::failbit);
        file.open(filename);
    } catch ( std::exception& e ) {
        fprintf(stderr, "ERROR: Cannot open file \"%s\".\n", filename);
        std::exit(EXIT_FAILURE);
    }
    std::stringstream shader;
    shader << file.rdbuf();
    std::string str = shader.str();
    const GLchar* shader_string = str.c_str();
    const GLint   shader_string_length = static_cast<GLint>( str.length() );

    // Define o código do shader GLSL, contido na string "shader_string"
    glShaderSource(shader_id, 1, &shader_string, &shader_string_length);

    // Compila o código do shader GLSL (em tempo de execução)
    glCompileShader(shader_id);

    // Verificamos se ocorreu algum erro ou "warning" durante a compilação
    GLint compiled_ok;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_ok);

    GLint log_length = 0;
    glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);

    // Alocamos memória para guardar o log de compilação.
    // A chamada "new" em C++ é equivalente ao "malloc()" do C.
    GLchar* log = new GLchar[log_length];
    glGetShaderInfoLog(shader_id, log_length, &log_length, log);

    // Imprime no terminal qualquer erro ou "warning" de compilação
    if ( log_length != 0 )
    {
        std::string  output;

        if ( !compiled_ok )
        {
            output += "ERROR: OpenGL compilation of \"";
            output += filename;
            output += "\" failed.\n";
            output += "== Start of compilation log\n";
            output += log;
            output += "== End of compilation log\n";
        }
        else
        {
            output += "WARNING: OpenGL compilation of \"";
            output += filename;
            output += "\".\n";
            output += "== Start of compilation log\n";
            output += log;
            output += "== End of compilation log\n";
        }

        fprintf(stderr, "%s", output.c_str());
    }

    // A chamada "delete" em C++ é equivalente ao "free()" do C
    delete [] log;
}

// Esta função cria um programa de GPU, o qual contém obrigatoriamente um
// Vertex Shader e um Fragment Shader.
GLuint CreateGpuProgram(GLuint vertex_shader_id, GLuint fragment_shader_id)
{
    // Criamos um identificador (ID) para este programa de GPU
    GLuint program_id = glCreateProgram();

    // Definição dos dois shaders GLSL que devem ser executados pelo programa
    glAttachShader(program_id, vertex_shader_id);
    glAttachShader(program_id, fragment_shader_id);

    // Linkagem dos shaders acima ao programa
    glLinkProgram(program_id);

    // Verificamos se ocorreu algum erro durante a linkagem
    GLint linked_ok = GL_FALSE;
    glGetProgramiv(program_id, GL_LINK_STATUS, &linked_ok);

    // Imprime no terminal qualquer erro de linkagem
    if ( linked_ok == GL_FALSE )
    {
        GLint log_length = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_length);

        // Alocamos memória para guardar o log de compilação.
        // A chamada "new" em C++ é equivalente ao "malloc()" do C.
        GLchar* log = new GLchar[log_length];

        glGetProgramInfoLog(program_id, log_length, &log_length, log);

        std::string output;

        output += "ERROR: OpenGL linking of program failed.\n";
        output += "== Start of link log\n";
        output += log;
        output += "\n== End of link log\n";

        // A chamada "delete" em C++ é equivalente ao "free()" do C
        delete [] log;

        fprintf(stderr, "%s", output.c_str());
    }

    // Os "Shader Objects" podem ser marcados para deleção após serem linkados
    glDeleteShader(vertex_shader_id);
    glDeleteShader(fragment_shader_id);

    // Retornamos o ID gerado acima
    return program_id;
}

// Definição da função que será chamada sempre que a janela do sistema
// operacional for redimensionada, por consequência alterando o tamanho do
// "framebuffer" (região de memória onde são armazenados os pixels da imagem).
void FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    // Indicamos que queremos renderizar em toda região do framebuffer. A
    // função "glViewport" define o mapeamento das "normalized device
    // coordinates" (NDC) para "pixel coordinates".  Essa é a operação de
    // "Screen Mapping" ou "Viewport Mapping" vista em aula (slides 33-44 do documento "Aula_07_Transformacoes_Geometricas_3D.pdf").
    glViewport(0, 0, width, height);

    // Atualizamos também a razão que define a proporção da janela (largura /
    // altura), a qual será utilizada na definição das matrizes de projeção,
    // tal que não ocorra distorções durante o processo de "Screen Mapping"
    // acima, quando NDC é mapeado para coordenadas de pixels. Veja slide 227 do documento "Aula_09_Projecoes.pdf".
    //
    // O cast para float é necessário pois números inteiros são arredondados ao
    // serem divididos!
    g_ScreenRatio = (float)width / height;
}

// Variáveis globais que armazenam a última posição do cursor do mouse, para
// que possamos calcular quanto que o mouse se movimentou entre dois instantes
// de tempo. Utilizadas no callback CursorPosCallback() abaixo.
double g_LastCursorPosX, g_LastCursorPosY;

// Função callback chamada sempre que o usuário aperta algum dos botões do mouse
void MouseButtonCallback(GLFWwindow* window, int key, int action, int mods)
{

    if (key == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
        // Se o usuário pressionou o botão esquerdo do mouse, guardamos a
        // posição atual do cursor nas variáveis g_LastCursorPosX e
        // g_LastCursorPosY.  Também, setamos a variável
        // g_LeftMouseButtonPressed como true, para saber que o usuário está
        // com o botão esquerdo pressionado.
        glfwGetCursorPos(window, &g_LastCursorPosX, &g_LastCursorPosY);
        g_LeftMouseButtonPressed = true;
    }
    if (key == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
        // Quando o usuário soltar o botão esquerdo do mouse, atualizamos a
        // variável abaixo para false.
        g_LeftMouseButtonPressed = false;
    }
    if (key == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
    {
        // Se o usuário pressionou o botão esquerdo do mouse, guardamos a
        // posição atual do cursor nas variáveis g_LastCursorPosX e
        // g_LastCursorPosY.  Também, setamos a variável
        // g_RightMouseButtonPressed como true, para saber que o usuário está
        // com o botão esquerdo pressionado.
        glfwGetCursorPos(window, &g_LastCursorPosX, &g_LastCursorPosY);
        g_RightMouseButtonPressed = true;
    }
    if (key == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
    {
        // Quando o usuário soltar o botão esquerdo do mouse, atualizamos a
        // variável abaixo para false.
        g_RightMouseButtonPressed = false;
    }
    if (key == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
    {
        // Se o usuário pressionou o botão esquerdo do mouse, guardamos a
        // posição atual do cursor nas variáveis g_LastCursorPosX e
        // g_LastCursorPosY.  Também, setamos a variável
        // g_MiddleMouseButtonPressed como true, para saber que o usuário está
        // com o botão esquerdo pressionado.
        glfwGetCursorPos(window, &g_LastCursorPosX, &g_LastCursorPosY);
        g_MiddleMouseButtonPressed = true;
    }
    if (key == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE)
    {
        // Quando o usuário soltar o botão esquerdo do mouse, atualizamos a
        // variável abaixo para false.
        g_MiddleMouseButtonPressed = false;
    }
}

// Função callback chamada sempre que o usuário movimentar o cursor do mouse em
// cima da janela OpenGL.
void CursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
    // Abaixo executamos o seguinte: caso o botão esquerdo do mouse esteja
    // pressionado, computamos quanto que o mouse se movimento desde o último
    // instante de tempo, e usamos esta movimentação para atualizar os
    // parâmetros que definem a posição da câmera dentro da cena virtual.
    // Assim, temos que o usuário consegue controlar a câmera.

    if (g_LeftMouseButtonPressed)
    {
        // Deslocamento do cursor do mouse em x e y de coordenadas de tela!
        float dx = xpos - g_LastCursorPosX;
        float dy = ypos - g_LastCursorPosY;

        // Atualizamos parâmetros da câmera com os deslocamentos
        g_CameraTheta -= 0.01f*dx;
        g_CameraPhi   += 0.01f*dy;

        // Em coordenadas esféricas, o ângulo phi deve ficar entre -pi/2 e +pi/2.
        float phimax = 3.141592f/2;
        float phimin = -phimax;

        if (g_CameraPhi > phimax)
            g_CameraPhi = phimax;

        if (g_CameraPhi < phimin)
            g_CameraPhi = phimin;

        // Atualizamos as variáveis globais para armazenar a posição atual do
        // cursor como sendo a última posição conhecida do cursor.
        g_LastCursorPosX = xpos;
        g_LastCursorPosY = ypos;
    }

    if (g_RightMouseButtonPressed)
    {
        // Deslocamento do cursor do mouse em x e y de coordenadas de tela!
        float dx = xpos - g_LastCursorPosX;
        float dy = ypos - g_LastCursorPosY;

        // Atualizamos parâmetros da antebraço com os deslocamentos
        g_ForearmAngleZ -= 0.01f*dx;
        g_ForearmAngleX += 0.01f*dy;

        // Atualizamos as variáveis globais para armazenar a posição atual do
        // cursor como sendo a última posição conhecida do cursor.
        g_LastCursorPosX = xpos;
        g_LastCursorPosY = ypos;
    }

    if (g_MiddleMouseButtonPressed)
    {
        // Deslocamento do cursor do mouse em x e y de coordenadas de tela!
        float dx = xpos - g_LastCursorPosX;
        float dy = ypos - g_LastCursorPosY;

        // Atualizamos parâmetros da antebraço com os deslocamentos
        g_TorsoPositionX += 0.01f*dx;
        g_TorsoPositionY -= 0.01f*dy;

        // Atualizamos as variáveis globais para armazenar a posição atual do
        // cursor como sendo a última posição conhecida do cursor.
        g_LastCursorPosX = xpos;
        g_LastCursorPosY = ypos;
    }
}

// Função callback chamada sempre que o usuário movimenta a "rodinha" do mouse.
void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    // Atualizamos a distância da câmera para a origem utilizando a
    // movimentação da "rodinha", simulando um ZOOM.
    g_CameraDistance -= 0.5f*yoffset;

    // Uma câmera look-at nunca pode estar exatamente "em cima" do ponto para
    // onde ela está olhando, pois isto gera problemas de divisão por zero na
    // definição do sistema de coordenadas da câmera. Isto é, a variável abaixo
    // nunca pode ser zero. Versões anteriores deste código possuíam este bug,
    // o qual foi detectado pelo aluno Vinicius Fraga (2017/2).
    const float verysmallnumber = std::numeric_limits<float>::epsilon();
    if (g_CameraDistance < verysmallnumber)
        g_CameraDistance = verysmallnumber;
}

// Definição da função que será chamada sempre que o usuário pressionar alguma
// tecla do teclado. Veja http://www.glfw.org/docs/latest/input_guide.html#input_key
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mod)
{
    // ==============
    // Não modifique este loop! Ele é utilizando para correção automatizada dos
    // laboratórios. Deve ser sempre o primeiro comando desta função KeyCallback().
    for (int i = 0; i < 10; ++i)
        if (key == GLFW_KEY_0 + i && action == GLFW_PRESS && mod == GLFW_MOD_SHIFT)
            std::exit(100 + i);
    // ==============

    if (key == GLFW_KEY_Y && action == GLFW_PRESS){
        printf("APERTOU O Y: Rotacionando com base no eixo Y\n");
        tecla_Y = true;
    }

    if(key == GLFW_KEY_0 && action == GLFW_PRESS){
        printf("APERTOU A TECLA DE SUBTRACAO: Diminui o tamanho do jogador\n");
        tecla_diminuir = true;
    }

    if(key == GLFW_KEY_1 && action == GLFW_PRESS){
        printf("APERTOU A TECLA DE SOMA: Aumenta o tamanho do jogador\n");
        tecla_aumentar = true;
    }

    if(key == GLFW_KEY_2 && action == GLFW_PRESS){
        printf("APERTOU A TECLA DE TAMANHO PADRAO: o tamanho do jogador volta ao normal\n");
        tecla_tamanho_padrao = true;
    }


     // Se o usuário apertar a tecla da seta para cima
     if (key == GLFW_KEY_UP && action == GLFW_PRESS){
        printf("APERTOU PARA CIMA\n");
        seta_para_cima = true;
    }


        // Se o usuário apertar a tecla da seta para direita
        if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
        {
            printf("APERTOU PARA DIREITA\n");
            seta_para_direita = true;
        }

        // Se o usuário apertar a tecla da seta para esquerda
        if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
        {
            printf("APERTOU PARA ESQUERDA\n");
            seta_para_esquerda = true;
        }

        // Se o usuário apertar a tecla da seta para baixo
        if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
        {
            printf("APERTOU PARA BAIXO\n");
            seta_para_baixo = true;
        }

        if (key == GLFW_KEY_SPACE && action == GLFW_PRESS){
            printf("APERTOU O ESPACO: Largar a bomba na frente do jogador\n");
            tecla_espaco = true;
        }

         if (key == GLFW_KEY_A && action == GLFW_PRESS){
            printf("APERTOU O A: Largar a bomba na esquerda do jogador\n");
            largar_bomba_esquerda = true;
        }

        if (key == GLFW_KEY_S && action == GLFW_PRESS){
            printf("APERTOU O S: Largar a bomba na direita do jogador\n");
            largar_bomba_direita = true;
        }

        if (key == GLFW_KEY_Z && action == GLFW_PRESS){
            printf("APERTOU O Z: Largar a bomba na frente do jogador\n");
            largar_bomba_frente = true;
        }

        if (key == GLFW_KEY_W && action == GLFW_PRESS){
            printf("APERTOU O W: Largar a bomba atrás do jogador\n");
            largar_bomba_atras = true;
        }



    // Se o usuário pressionar a tecla ESC, fechamos a janela.
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    // O código abaixo implementa a seguinte lógica:
    //   Se apertar tecla X       então g_AngleX += delta;
    //   Se apertar tecla shift+X então g_AngleX -= delta;
    //   Se apertar tecla Y       então g_AngleY += delta;
    //   Se apertar tecla shift+Y então g_AngleY -= delta;
    //   Se apertar tecla Z       então g_AngleZ += delta;
    //   Se apertar tecla shift+Z então g_AngleZ -= delta;

    float delta = 3.141592 / 16; // 22.5 graus, em radianos.

    if (key == GLFW_KEY_X && action == GLFW_PRESS)
    {
        g_AngleX += (mod & GLFW_MOD_SHIFT) ? -delta : delta;
    }

    if (key == GLFW_KEY_Y && action == GLFW_PRESS)
    {
        g_AngleY += (mod & GLFW_MOD_SHIFT) ? -delta : delta;
    }
    if (key == GLFW_KEY_Z && action == GLFW_PRESS)
    {
        g_AngleZ += (mod & GLFW_MOD_SHIFT) ? -delta : delta;
    }

    // Se o usuário apertar a tecla espaço, resetamos os ângulos de Euler para zero.
    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    {
        g_AngleX = 0.0f;
        g_AngleY = 0.0f;
        g_AngleZ = 0.0f;
        g_ForearmAngleX = 0.0f;
        g_ForearmAngleZ = 0.0f;
        g_TorsoPositionX = 0.0f;
        g_TorsoPositionY = 0.0f;
    }

    // Se o usuário apertar a tecla P, utilizamos projeção perspectiva.
    if (key == GLFW_KEY_P && action == GLFW_PRESS)
    {
        g_UsePerspectiveProjection = true;
    }

    // Se o usuário apertar a tecla O, utilizamos projeção ortográfica.
    if (key == GLFW_KEY_O && action == GLFW_PRESS)
    {
        g_UsePerspectiveProjection = false;
    }

    // Se o usuário apertar a tecla H, fazemos um "toggle" do texto informativo mostrado na tela.
    if (key == GLFW_KEY_H && action == GLFW_PRESS)
    {
        g_ShowInfoText = !g_ShowInfoText;
    }

    // Se o usuário apertar a tecla R, recarregamos os shaders dos arquivos "shader_fragment.glsl" e "shader_vertex.glsl".
    if (key == GLFW_KEY_R && action == GLFW_PRESS)
    {
        LoadShadersFromFiles();
        fprintf(stdout,"Shaders recarregados!\n");
        fflush(stdout);
    }
}

// Definimos o callback para impressão de erros da GLFW no terminal
void ErrorCallback(int error, const char* description)
{
    fprintf(stderr, "ERROR: GLFW: %s\n", description);
}

// Esta função recebe um vértice com coordenadas de modelo p_model e passa o
// mesmo por todos os sistemas de coordenadas armazenados nas matrizes model,
// view, e projection; e escreve na tela as matrizes e pontos resultantes
// dessas transformações.
void TextRendering_ShowModelViewProjection(
    GLFWwindow* window,
    glm::mat4 projection,
    glm::mat4 view,
    glm::mat4 model,
    glm::vec4 p_model
)
{
    if ( !g_ShowInfoText )
        return;

    glm::vec4 p_world = model*p_model;
    glm::vec4 p_camera = view*p_world;
    glm::vec4 p_clip = projection*p_camera;
    glm::vec4 p_ndc = p_clip / p_clip.w;

    float pad = TextRendering_LineHeight(window);

    TextRendering_PrintString(window, " Model matrix             Model     In World Coords.", -1.0f, 1.0f-pad, 1.0f);
    TextRendering_PrintMatrixVectorProduct(window, model, p_model, -1.0f, 1.0f-2*pad, 1.0f);

    TextRendering_PrintString(window, "                                        |  ", -1.0f, 1.0f-6*pad, 1.0f);
    TextRendering_PrintString(window, "                            .-----------'  ", -1.0f, 1.0f-7*pad, 1.0f);
    TextRendering_PrintString(window, "                            V              ", -1.0f, 1.0f-8*pad, 1.0f);

    TextRendering_PrintString(window, " View matrix              World     In Camera Coords.", -1.0f, 1.0f-9*pad, 1.0f);
    TextRendering_PrintMatrixVectorProduct(window, view, p_world, -1.0f, 1.0f-10*pad, 1.0f);

    TextRendering_PrintString(window, "                                        |  ", -1.0f, 1.0f-14*pad, 1.0f);
    TextRendering_PrintString(window, "                            .-----------'  ", -1.0f, 1.0f-15*pad, 1.0f);
    TextRendering_PrintString(window, "                            V              ", -1.0f, 1.0f-16*pad, 1.0f);

    TextRendering_PrintString(window, " Projection matrix        Camera                    In NDC", -1.0f, 1.0f-17*pad, 1.0f);
    TextRendering_PrintMatrixVectorProductDivW(window, projection, p_camera, -1.0f, 1.0f-18*pad, 1.0f);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    glm::vec2 a = glm::vec2(-1, -1);
    glm::vec2 b = glm::vec2(+1, +1);
    glm::vec2 p = glm::vec2( 0,  0);
    glm::vec2 q = glm::vec2(width, height);

    glm::mat4 viewport_mapping = Matrix(
        (q.x - p.x)/(b.x-a.x), 0.0f, 0.0f, (b.x*p.x - a.x*q.x)/(b.x-a.x),
        0.0f, (q.y - p.y)/(b.y-a.y), 0.0f, (b.y*p.y - a.y*q.y)/(b.y-a.y),
        0.0f , 0.0f , 1.0f , 0.0f ,
        0.0f , 0.0f , 0.0f , 1.0f
    );

    TextRendering_PrintString(window, "                                                       |  ", -1.0f, 1.0f-22*pad, 1.0f);
    TextRendering_PrintString(window, "                            .--------------------------'  ", -1.0f, 1.0f-23*pad, 1.0f);
    TextRendering_PrintString(window, "                            V                           ", -1.0f, 1.0f-24*pad, 1.0f);

    TextRendering_PrintString(window, " Viewport matrix           NDC      In Pixel Coords.", -1.0f, 1.0f-25*pad, 1.0f);
    TextRendering_PrintMatrixVectorProductMoreDigits(window, viewport_mapping, p_ndc, -1.0f, 1.0f-26*pad, 1.0f);
}

// Escrevemos na tela os ângulos de Euler definidos nas variáveis globais
// g_AngleX, g_AngleY, e g_AngleZ.
void TextRendering_ShowEulerAngles(GLFWwindow* window)
{
    if ( !g_ShowInfoText )
        return;

    float pad = TextRendering_LineHeight(window);

    char buffer[80];
    snprintf(buffer, 80, "Euler Angles rotation matrix = Z(%.2f)*Y(%.2f)*X(%.2f)\n", g_AngleZ, g_AngleY, g_AngleX);

    TextRendering_PrintString(window, buffer, -1.0f+pad/10, -1.0f+2*pad/10, 1.0f);
}

// Escrevemos na tela qual matriz de projeção está sendo utilizada.
void TextRendering_ShowProjection(GLFWwindow* window)
{
    if ( !g_ShowInfoText )
        return;

    float lineheight = TextRendering_LineHeight(window);
    float charwidth = TextRendering_CharWidth(window);

    if ( g_UsePerspectiveProjection )
        TextRendering_PrintString(window, "Perspective", 1.0f-13*charwidth, -1.0f+2*lineheight/10, 1.0f);
    else
        TextRendering_PrintString(window, "Orthographic", 1.0f-13*charwidth, -1.0f+2*lineheight/10, 1.0f);
}

// Escrevemos na tela o número de quadros renderizados por segundo (frames per
// second).
void TextRendering_ShowFramesPerSecond(GLFWwindow* window)
{
    if ( !g_ShowInfoText )
        return;

    // Variáveis estáticas (static) mantém seus valores entre chamadas
    // subsequentes da função!
    static float old_seconds = (float)glfwGetTime();
    static int   ellapsed_frames = 0;
    static char  buffer[20] = "?? fps";
    static int   numchars = 7;

    ellapsed_frames += 1;

    // Recuperamos o número de segundos que passou desde a execução do programa
    float seconds = (float)glfwGetTime();

    // Número de segundos desde o último cálculo do fps
    float ellapsed_seconds = seconds - old_seconds;

    if ( ellapsed_seconds > 1.0f )
    {
        numchars = snprintf(buffer, 20, "%.2f fps", ellapsed_frames / ellapsed_seconds);

        old_seconds = seconds;
        ellapsed_frames = 0;
    }

    float lineheight = TextRendering_LineHeight(window);
    float charwidth = TextRendering_CharWidth(window);

    TextRendering_PrintString(window, buffer, 1.0f-(numchars + 1)*charwidth, 1.0f-lineheight, 1.0f);
}

// Função para debugging: imprime no terminal todas informações de um modelo
// geométrico carregado de um arquivo ".obj".
// Veja: https://github.com/syoyo/tinyobjloader/blob/22883def8db9ef1f3ffb9b404318e7dd25fdbb51/loader_example.cc#L98
void PrintObjModelInfo(ObjModel* model)
{
  const tinyobj::attrib_t                & attrib    = model->attrib;
  const std::vector<tinyobj::shape_t>    & shapes    = model->shapes;
  const std::vector<tinyobj::material_t> & materials = model->materials;

  printf("# of vertices  : %d\n", (int)(attrib.vertices.size() / 3));
  printf("# of normals   : %d\n", (int)(attrib.normals.size() / 3));
  printf("# of texcoords : %d\n", (int)(attrib.texcoords.size() / 2));
  printf("# of shapes    : %d\n", (int)shapes.size());
  printf("# of materials : %d\n", (int)materials.size());

  for (size_t v = 0; v < attrib.vertices.size() / 3; v++) {
    printf("  v[%ld] = (%f, %f, %f)\n", static_cast<long>(v),
           static_cast<const double>(attrib.vertices[3 * v + 0]),
           static_cast<const double>(attrib.vertices[3 * v + 1]),
           static_cast<const double>(attrib.vertices[3 * v + 2]));
  }

  for (size_t v = 0; v < attrib.normals.size() / 3; v++) {
    printf("  n[%ld] = (%f, %f, %f)\n", static_cast<long>(v),
           static_cast<const double>(attrib.normals[3 * v + 0]),
           static_cast<const double>(attrib.normals[3 * v + 1]),
           static_cast<const double>(attrib.normals[3 * v + 2]));
  }

  for (size_t v = 0; v < attrib.texcoords.size() / 2; v++) {
    printf("  uv[%ld] = (%f, %f)\n", static_cast<long>(v),
           static_cast<const double>(attrib.texcoords[2 * v + 0]),
           static_cast<const double>(attrib.texcoords[2 * v + 1]));
  }

  // For each shape
  for (size_t i = 0; i < shapes.size(); i++) {
    printf("shape[%ld].name = %s\n", static_cast<long>(i),
           shapes[i].name.c_str());
    printf("Size of shape[%ld].indices: %lu\n", static_cast<long>(i),
           static_cast<unsigned long>(shapes[i].mesh.indices.size()));

    size_t index_offset = 0;

    assert(shapes[i].mesh.num_face_vertices.size() ==
           shapes[i].mesh.material_ids.size());

    printf("shape[%ld].num_faces: %lu\n", static_cast<long>(i),
           static_cast<unsigned long>(shapes[i].mesh.num_face_vertices.size()));

    // For each face
    for (size_t f = 0; f < shapes[i].mesh.num_face_vertices.size(); f++) {
      size_t fnum = shapes[i].mesh.num_face_vertices[f];

      printf("  face[%ld].fnum = %ld\n", static_cast<long>(f),
             static_cast<unsigned long>(fnum));

      // For each vertex in the face
      for (size_t v = 0; v < fnum; v++) {
        tinyobj::index_t idx = shapes[i].mesh.indices[index_offset + v];
        printf("    face[%ld].v[%ld].idx = %d/%d/%d\n", static_cast<long>(f),
               static_cast<long>(v), idx.vertex_index, idx.normal_index,
               idx.texcoord_index);
      }

      printf("  face[%ld].material_id = %d\n", static_cast<long>(f),
             shapes[i].mesh.material_ids[f]);

      index_offset += fnum;
    }

    printf("shape[%ld].num_tags: %lu\n", static_cast<long>(i),
           static_cast<unsigned long>(shapes[i].mesh.tags.size()));
    for (size_t t = 0; t < shapes[i].mesh.tags.size(); t++) {
      printf("  tag[%ld] = %s ", static_cast<long>(t),
             shapes[i].mesh.tags[t].name.c_str());
      printf(" ints: [");
      for (size_t j = 0; j < shapes[i].mesh.tags[t].intValues.size(); ++j) {
        printf("%ld", static_cast<long>(shapes[i].mesh.tags[t].intValues[j]));
        if (j < (shapes[i].mesh.tags[t].intValues.size() - 1)) {
          printf(", ");
        }
      }
      printf("]");

      printf(" floats: [");
      for (size_t j = 0; j < shapes[i].mesh.tags[t].floatValues.size(); ++j) {
        printf("%f", static_cast<const double>(
                         shapes[i].mesh.tags[t].floatValues[j]));
        if (j < (shapes[i].mesh.tags[t].floatValues.size() - 1)) {
          printf(", ");
        }
      }
      printf("]");

      printf(" strings: [");
      for (size_t j = 0; j < shapes[i].mesh.tags[t].stringValues.size(); ++j) {
        printf("%s", shapes[i].mesh.tags[t].stringValues[j].c_str());
        if (j < (shapes[i].mesh.tags[t].stringValues.size() - 1)) {
          printf(", ");
        }
      }
      printf("]");
      printf("\n");
    }
  }

  for (size_t i = 0; i < materials.size(); i++) {
    printf("material[%ld].name = %s\n", static_cast<long>(i),
           materials[i].name.c_str());
    printf("  material.Ka = (%f, %f ,%f)\n",
           static_cast<const double>(materials[i].ambient[0]),
           static_cast<const double>(materials[i].ambient[1]),
           static_cast<const double>(materials[i].ambient[2]));
    printf("  material.Kd = (%f, %f ,%f)\n",
           static_cast<const double>(materials[i].diffuse[0]),
           static_cast<const double>(materials[i].diffuse[1]),
           static_cast<const double>(materials[i].diffuse[2]));
    printf("  material.Ks = (%f, %f ,%f)\n",
           static_cast<const double>(materials[i].specular[0]),
           static_cast<const double>(materials[i].specular[1]),
           static_cast<const double>(materials[i].specular[2]));
    printf("  material.Tr = (%f, %f ,%f)\n",
           static_cast<const double>(materials[i].transmittance[0]),
           static_cast<const double>(materials[i].transmittance[1]),
           static_cast<const double>(materials[i].transmittance[2]));
    printf("  material.Ke = (%f, %f ,%f)\n",
           static_cast<const double>(materials[i].emission[0]),
           static_cast<const double>(materials[i].emission[1]),
           static_cast<const double>(materials[i].emission[2]));
    printf("  material.Ns = %f\n",
           static_cast<const double>(materials[i].shininess));
    printf("  material.Ni = %f\n", static_cast<const double>(materials[i].ior));
    printf("  material.dissolve = %f\n",
           static_cast<const double>(materials[i].dissolve));
    printf("  material.illum = %d\n", materials[i].illum);
    printf("  material.map_Ka = %s\n", materials[i].ambient_texname.c_str());
    printf("  material.map_Kd = %s\n", materials[i].diffuse_texname.c_str());
    printf("  material.map_Ks = %s\n", materials[i].specular_texname.c_str());
    printf("  material.map_Ns = %s\n",
           materials[i].specular_highlight_texname.c_str());
    printf("  material.map_bump = %s\n", materials[i].bump_texname.c_str());
    printf("  material.map_d = %s\n", materials[i].alpha_texname.c_str());
    printf("  material.disp = %s\n", materials[i].displacement_texname.c_str());
    printf("  <<PBR>>\n");
    printf("  material.Pr     = %f\n", materials[i].roughness);
    printf("  material.Pm     = %f\n", materials[i].metallic);
    printf("  material.Ps     = %f\n", materials[i].sheen);
    printf("  material.Pc     = %f\n", materials[i].clearcoat_thickness);
    printf("  material.Pcr    = %f\n", materials[i].clearcoat_thickness);
    printf("  material.aniso  = %f\n", materials[i].anisotropy);
    printf("  material.anisor = %f\n", materials[i].anisotropy_rotation);
    printf("  material.map_Ke = %s\n", materials[i].emissive_texname.c_str());
    printf("  material.map_Pr = %s\n", materials[i].roughness_texname.c_str());
    printf("  material.map_Pm = %s\n", materials[i].metallic_texname.c_str());
    printf("  material.map_Ps = %s\n", materials[i].sheen_texname.c_str());
    printf("  material.norm   = %s\n", materials[i].normal_texname.c_str());
    std::map<std::string, std::string>::const_iterator it(
        materials[i].unknown_parameter.begin());
    std::map<std::string, std::string>::const_iterator itEnd(
        materials[i].unknown_parameter.end());

    for (; it != itEnd; it++) {
      printf("  material.%s = %s\n", it->first.c_str(), it->second.c_str());
    }
    printf("\n");
  }
}

// set makeprg=cd\ ..\ &&\ make\ run\ >/dev/null
// vim: set spell spelllang=pt_br :

