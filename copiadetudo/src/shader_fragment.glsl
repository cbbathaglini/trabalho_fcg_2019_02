#version 330 core

// Atributos de fragmentos recebidos como entrada ("in") pelo Fragment Shader.
// Neste exemplo, este atributo foi gerado pelo rasterizador como a
// interpolação da posição global e a normal de cada vértice, definidas em
// "shader_vertex.glsl" e "main.cpp".
in vec4 position_world;
in vec4 normal;

// Posição do vértice atual no sistema de coordenadas local do modelo.
in vec4 position_model;

// Coordenadas de textura obtidas do arquivo OBJ (se existirem!)
in vec2 texcoords;

// Matrizes computadas no código C++ e enviadas para a GPU
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

// Identificador que define qual objeto está sendo desenhado no momento
//sobre os objetos da cena
#define APARENCIA_JOGADOR_1 0
#define APARENCIA_JOGADOR_2 1
#define PLANE  2
#define CUBO  3
#define CAIXA  4
#define CHAO_PEDRAS  5
#define PAREDES_FUNDO  6
#define PAREDES  7
uniform int object_id;

// Parâmetros da axis-aligned bounding box (AABB) do modelo
uniform vec4 bbox_min;
uniform vec4 bbox_max;

// Variáveis para acesso das imagens de textura
uniform sampler2D TextureImage0;
uniform sampler2D TextureImage1;
uniform sampler2D TextureImage2;
uniform sampler2D TextureImage3;
uniform sampler2D TextureImage4;
uniform sampler2D TextureImage5;




// O valor de saída ("out") de um Fragment Shader é a cor final do fragmento.
out vec3 color;

// Constantes
#define M_PI   3.14159265358979323846
#define M_PI_2 1.57079632679489661923
const float PI = acos(float(-1));
const float HALF_PI = PI / 2;
const float DOUBLE_PI = 2 * PI;
bool tem_textura = false; // Variável booleana auxiliar para identificar objetos com texturas
void main()
{
    // Obtemos a posição da câmera utilizando a inversa da matriz que define o
    // sistema de coordenadas da câmera.
    vec4 origin = vec4(0.0, 0.0, 0.0, 1.0);
    vec4 camera_position = inverse(view) * origin;


    // O fragmento atual é coberto por um ponto que percente à superfície de um
    // dos objetos virtuais da cena. Este ponto, p, possui uma posição no
    // sistema de coordenadas global (World coordinates). Esta posição é obtida
    // através da interpolação, feita pelo rasterizador, da posição de cada
    // vértice.
    vec4 p = position_world;

    //flashlight: fonte de luz é o ponto da câmera,
    //e a direção da fonte é a direção para onde a câmera aponta (como uma lanterna)
    vec4 flashlightPosition = camera_position;
    vec4 flashlightDirection = normalize(camera_position - position_world);

    // Normal do fragmento atual, interpolada pelo rasterizador a partir das
    // normais de cada vértice.
    vec4 n = normalize(normal);

    // Vetor que define o sentido da fonte de luz em relação ao ponto atual.
    vec4 l = normalize(vec4(1.0,1.0,0.0,0.0));

    // Vetor que define o sentido da câmera em relação ao ponto atual.
    vec4 v = normalize(camera_position - p);

    // Coordenadas de textura U e V
    float U = 0.0;
    float V = 0.0;

    // Vetor que define o sentido da reflexão especular ideal.
    vec4 r = -l + 2*n*dot(n,l);

    // Parâmetros que definem as propriedades espectrais da superfície
    vec3 Kd; // Refletância difusa
    vec3 Ks; // Refletância especular
    vec3 Ka; // Refletância ambiente
    float q; // Expoente especular para o modelo de iluminação de Phong



    if ( object_id == APARENCIA_JOGADOR_1 )
    {
        Kd = vec3(0.0,0.8,0.8);
        Ks = vec3(1.0,1.0,1.0);
        Ka = vec3(0.04,0.2,0.4);
        q = 32.0;
    }else if ( object_id == APARENCIA_JOGADOR_2 ){
        Kd = vec3(0.8,0.4,0.08);
        Ks = vec3(0.3,0.3,0.3);
        Ka = vec3(0.4,0.2,0.04);
        q = 1.0;
    }else if ( object_id == PLANE ){
        tem_textura = true;
        U = texcoords.x;
        V = texcoords.y;


        Kd = texture(TextureImage2, vec2(U,V)).rgb;
        Ks = vec3(0.1,0.3,0.1);
        Ka = vec3(0.3,0.8,0.2);
        q = 20.0;
    }else if ( object_id == PAREDES_FUNDO ){

        tem_textura = true;
        U = texcoords.x;
        V = texcoords.y;


        Kd = texture(TextureImage4, vec2(U,V)).rgb;
        Ks = vec3(0.1,0.3,0.1);
        Ka = vec3(0.3,0.8,0.2);
        q = 20.0;

    }else if ( object_id == PAREDES){
        U = texcoords.x;
        V = texcoords.y;

        tem_textura = true;
        Kd = texture(TextureImage1, vec2(U,V)).rgb;
        Ks = vec3(0.1,0.1,0.1);
        Ka = vec3(0.8,0.1,0.1);
        q = 20.0;
    }else if ( object_id == CHAO_PEDRAS ){
        U = texcoords.x;
        V = texcoords.y;
        tem_textura = true;
        Kd = texture(TextureImage1, vec2(U,V)).rgb;
        Ks = vec3(0.0,0.0,0.0);
        Ka = vec3(0.1,0.1,0.0);
        q = 20.0;
    }else if ( object_id == CAIXA){
        U = texcoords.x;
        V = texcoords.y;

        tem_textura = true;
        Kd = texture(TextureImage0, vec2(U,V)).rgb;
        Ks = vec3(0.0,0.0,0.0);
        Ka = vec3(0.1,0.1,0.0);
        q = 20.0;
    }
    else{
        Kd = vec3(0.0,0.0,0.0);
        Ks = vec3(0.0,0.0,0.0);
        Ka = vec3(0.0,0.0,0.0);
        q = 1.0;
    }


    // Termo difuso utilizando a lei dos cossenos de Lambert
    float lambert_diffuse_term = max(0,dot(n,l)); // PREENCHA AQUI o termo difuso de Lambert

    // Termo especular utilizando o modelo de iluminação de Phong
    float phong_specular_term  = pow(max(0,dot(r,v)),q); // PREENCH AQUI o termo especular de Phong

    // Espectro da fonte de iluminação
    vec3 light_spectrum = vec3(1.0,1.0,1.0); // PREENCH AQUI o espectro da fonte de luz

    // Espectro da luz ambiente
    vec3 ambient_light_spectrum = vec3(0.2,0.2,0.2); // PREENCHA AQUI o espectro da luz ambiente


    // Cor final do fragmento calculada com uma combinação dos termos difuso,
    // especular, e ambiente. Veja slide 131 do documento "Aula_17_e_18_Modelos_de_Iluminacao.pdf".
    color = tem_textura ?
            (Kd * (pow(lambert_diffuse_term, 1) + 0.01)
                + Ka * ambient_light_spectrum
                + Ks * light_spectrum * phong_specular_term)
            : (Kd * light_spectrum * lambert_diffuse_term
                + Ka * ambient_light_spectrum
                + Ks * light_spectrum * phong_specular_term);



    // Cor final com correção gamma, considerando monitor sRGB.
    // Veja https://en.wikipedia.org/w/index.php?title=Gamma_correction&oldid=751281772#Windows.2C_Mac.2C_sRGB_and_TV.2Fvideo_standard_gammas
    color = pow(color, vec3(1.0,1.0,1.0)/2.2);
}

